<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('HtmlHeader')

<body>
{{--<div class="wrapper">--}}



@include('header')
{{--@include('Modal')--}}
{{--@include('headerMob')--}}


@yield('content')

@include('footer')

<script>
    $(document).ready(function() {
        @include('globalscript')
        @yield('script')
        // $(document).on('click', '.navigation', function() {
        //     $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
        // });
    });
</script>

</body>
</html>
