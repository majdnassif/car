<footer class="footer-bg">
    <!-- Footer Content -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3  col-sm-6 col-xs-12">
                    <!-- Info Widget -->
                    <div class="widget">
                        <div class="logo"> <img alt="" src="{{asset('theme/images/logo.png')}}"> </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur et dolor eget erat fringilla port.</p>
                        <ul class="apps-donwloads">
                            <li><img src="{{asset('theme/images/googleplay.png')}}" alt=""></li>
                            <li><img src="{{asset('theme/images/appstore.png')}}" alt=""></li>
                        </ul>
                    </div>
                    <!-- Info Widget Exit -->
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12">
                    <!-- Follow Us -->
                    <div class="widget socail-icons">
                        <h5>Follow Us</h5>
                        <ul>
                            <li><a class="Facebook" ><i class="fa fa-facebook"></i></a><span>Facebook</span></li>
                            <li><a class="Twitter" href=""><i class="fa fa-twitter"></i></a><span>Twitter</span></li>
                            <li><a class="Linkedin" href=""><i class="fa fa-linkedin"></i></a><span>Linkedin</span></li>
                            <li><a class="Google" href=""><i class="fa fa-google-plus"></i></a><span>Google+</span></li>
                        </ul>
                    </div>
                    <!-- Follow Us End -->
                </div>
                <div class="col-md-2  col-sm-6 col-xs-12">
                    <!-- Follow Us -->
                    <div class="widget my-quicklinks">
                        <h5>Quick Links</h5>
                        <ul >
                            <li><a href="{{route('about')}}">About Us</a></li>
                            <li><a href="{{route('faqs')}}">Faqs</a></li>
                            <li><a href="#">Packages</a></li>
                            <li><a href="{{route('contact')}}">Contact Us</a></li>
                        </ul>
                    </div>
                    <!-- Follow Us End -->
                </div>
                <div class="col-md-5  col-sm-6 col-xs-12">
                    <!-- Newslatter -->
                    <div class="widget widget-newsletter">
                        <h5>Singup for Weekly Newsletter</h5>
                        <div class="fieldset">
                            <p>We may send you information about related events, webinars, products and services which we believe.</p>
                            <form>
                                <input class="" value="Enter your email address" type="text">
                                <input class="submit-btn" name="submit" value="Submit" type="submit">
                            </form>
                        </div>
                    </div>
                    <div class="copyright">
                        <p>© 2017 Carspot All rights reserved. Design by <a href="http://themeforest.net/user/scriptsbundle/portfolio" target="_blank">Scriptsbundle</a> </p>
                    </div>
                    <!-- Newslatter -->
                </div>
            </div>
        </div>
    </div>

</footer>
<!-- =-=-=-=-=-=-= FOOTER END =-=-=-=-=-=-= -->
{{--</div>--}}
<!-- Main Content Area End -->
<!-- Back To Top -->
<a href="#0" class="cd-top">Top</a>
<!-- =-=-=-=-=-=-= JQUERY =-=-=-=-=-=-= -->
<script src="{{asset('theme/js/jquery.min.js')}}"></script>
<!-- Bootstrap Core Css  -->
<script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
<!-- Jquery Easing -->
<script src="{{asset('theme/js/easing.js')}}"></script>
<!-- Menu Hover  -->
<script src="{{asset('theme/js/carspot-menu.js')}}"></script>
<!-- Jquery Appear Plugin -->
<script src="{{asset('theme/js/jquery.appear.min.js')}}"></script>
<!-- Numbers Animation   -->
<script src="{{asset('theme/js/jquery.countTo.js')}}"></script>
<!-- Jquery Select Options  -->
<script src="{{asset('theme/js/select2.min.js')}}"></script>
<!-- noUiSlider -->
<script src="{{asset('theme/js/nouislider.all.min.js')}}"></script>
<!-- Carousel Slider  -->
<script src="{{asset('theme/js/carousel.min.js')}}"></script>
<script src="{{asset('theme/js/slide.js')}}"></script>
<!-- Image Loaded  -->
<script src="{{asset('theme/js/imagesloaded.js')}}"></script>
<script src="{{asset('theme/js/isotope.min.js')}}"></script>
<!-- CheckBoxes  -->
<script src="{{asset('theme/js/icheck.min.js')}}"></script>
<!-- Jquery Migration  -->
<script src="{{asset('theme/js/jquery-migrate.min.js')}}"></script>
<!-- Style Switcher -->
<script src="{{asset('theme/js/color-switcher.js')}}"></script>
<!-- PrettyPhoto -->
<script src="{{asset('theme/js/jquery.fancybox.min.js')}}"></script>
<!-- Wow Animation -->
<script src="{{asset('theme/js/wow.js')}}"></script>
<!-- Template Core JS -->
<script src="{{asset('theme/js/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('theme/js/script.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" data-version-4></script>
