@extends('welcome')

@section('content')


    <div class="background-rotator">
        <!-- slider start-->
        <div class="owl-carousel owl-theme background-rotator-slider">
            <!-- Slide -->
            <div class="item linear-overlay"><img src="{{asset('theme/images/slider/1.jpg')}}" alt=""></div>
            <!-- Slide -->
            <div class="item linear-overlay"><img  src="{{asset('theme/images/slider/2.jpg')}}" alt=""></div>
            <!-- Slide -->
            <div class="item linear-overlay"><img src="{{asset('theme/images/slider/3.jpg')}}" alt=""></div>
        </div>
        <div class="search-section">
            <!-- Find search section -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Heading -->
                        <div class="content">
                            <div class="heading-caption">
                                <h1>Find what are you looking for</h1>
                                <p>Over <strong>95,00,000</strong> Classified Ads Listing</p>
                            </div>
                            <div class="search-form">
                                <form>
                                    <div class="row">
                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                            <!-- Category -->
                                            <select class="model form-control">
                                                <option label="Select Option"></option>
                                                <option value="0">Cars & Bikes</option>
                                                <option value="1">Mobile Phones</option>
                                                <option value="2">Home Appliances</option>
                                                <option value="3">Clothing</option>
                                                <option value="4">Human Resource</option>
                                                <option value="5">Information Technology</option>
                                                <option value="6">Marketing</option>
                                                <option value="7">Others</option>
                                                <option value="8">Sales</option>
                                            </select>
                                        </div>
                                        <!-- Input Field -->
                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                            <input type="text" class="form-control" placeholder="What Are You Looking For..." />
                                        </div>
                                        <!-- Search Button -->
                                        <div class="col-md-4 col-xs-12 col-sm-4">
                                            <button type="submit" class="btn btn-theme btn-block">Search <i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.Find search section-->
    </div>
    <!-- =-=-=-=-=-=-= Background Rotator End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Featured =-=-=-=-=-=-= -->
        <section class="custom-padding white over-hidden">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <!-- Heading Area -->
                    <div class="heading-panel">
                        <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                            <!-- Main Title -->
                            <h1>Latest <span class="heading-color"> Featured</span> Cars</h1>
                            <!-- Short Description -->
                            <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                        </div>
                    </div>
                    <!-- Middle Content Box -->
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="row">
                            <div class="featured-slider container owl-carousel owl-theme">
                                <div class="item">
                                    <div class="grid-style-2">
                                        <!-- Listing Ad Grid -->
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="category-grid-box-1">
                                                <div class="featured-ribbon">
                                                    <span>Featured</span>
                                                </div>
                                                <div class="image">
                                                    <img alt="Carspot" src="{{asset('theme/images/posting/15.jpg')}}" class="img-responsive">
                                                    <div class="ribbon popular"></div>
                                                    <div class="price-tag">
                                                        <div class="price"><span>$205,000</span></div>
                                                    </div>
                                                </div>
                                                <div class="short-description-1 clearfix">
                                                    <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                                    <h3><a title="" href="single-page-listing.html">Honda Civic 2017 Sports Edition</a></h3>
                                                    <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                                    <ul class="list-unstyled">
                                                        <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                                    </ul>
                                                </div>
                                                <div class="ad-info-1">
                                                    <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                                    <ul class="pull-right">
                                                        <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                        <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- Listing Ad Grid -->
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="grid-style-2">
                                        <!-- Listing Ad Grid -->
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="category-grid-box-1">
                                                <div class="featured-ribbon">
                                                    <span>Featured</span>
                                                </div>
                                                <div class="image">
                                                    <img alt="Carspot" src="{{asset('theme/images/posting/2.jpg')}}" class="img-responsive">
                                                    <div class="ribbon popular"></div>
                                                    <div class="price-tag">
                                                        <div class="price"><span>$205,000</span></div>
                                                    </div>
                                                </div>
                                                <div class="short-description-1 clearfix">
                                                    <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                                    <h3><a title="" href="single-page-listing.html">Honda Civic 2017 Sports Edition</a></h3>
                                                    <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                                    <ul class="list-unstyled">
                                                        <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                                    </ul>
                                                </div>
                                                <div class="ad-info-1">
                                                    <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                                    <ul class="pull-right">
                                                        <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                        <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- Listing Ad Grid -->
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="grid-style-2">
                                        <!-- Listing Ad Grid -->
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="category-grid-box-1">
                                                <div class="featured-ribbon">
                                                    <span>Featured</span>
                                                </div>
                                                <div class="image">
                                                    <img alt="Carspot" src="{{asset('theme/images/posting/3.jpg')}}" class="img-responsive">
                                                    <div class="ribbon popular"></div>
                                                    <div class="price-tag">
                                                        <div class="price"><span>$205,000</span></div>
                                                    </div>
                                                </div>
                                                <div class="short-description-1 clearfix">
                                                    <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                                    <h3><a title="" href="single-page-listing.html">Honda Civic 2017 Sports Edition</a></h3>
                                                    <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                                    <ul class="list-unstyled">
                                                        <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                                    </ul>
                                                </div>
                                                <div class="ad-info-1">
                                                    <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                                    <ul class="pull-right">
                                                        <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                        <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- Listing Ad Grid -->
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="grid-style-2">
                                        <!-- Listing Ad Grid -->
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="category-grid-box-1">
                                                <div class="featured-ribbon">
                                                    <span>Featured</span>
                                                </div>
                                                <div class="image">
                                                    <img alt="Carspot" src="{{asset('theme/images/posting/4.jpg')}}" class="img-responsive">
                                                    <div class="ribbon popular"></div>
                                                    <div class="price-tag">
                                                        <div class="price"><span>$205,000</span></div>
                                                    </div>
                                                </div>
                                                <div class="short-description-1 clearfix">
                                                    <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                                    <h3><a title="" href="single-page-listing.html">Honda Civic 2017 Sports Edition</a></h3>
                                                    <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                                    <ul class="list-unstyled">
                                                        <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                                    </ul>
                                                </div>
                                                <div class="ad-info-1">
                                                    <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                                    <ul class="pull-right">
                                                        <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                        <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- Listing Ad Grid -->
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="grid-style-2">
                                        <!-- Listing Ad Grid -->
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="category-grid-box-1">
                                                <div class="featured-ribbon">
                                                    <span>Featured</span>
                                                </div>
                                                <div class="image">
                                                    <img alt="Carspot" src="{{asset('theme/images/posting/5.jpg')}}" class="img-responsive">
                                                    <div class="ribbon popular"></div>
                                                    <div class="price-tag">
                                                        <div class="price"><span>$205,000</span></div>
                                                    </div>
                                                </div>
                                                <div class="short-description-1 clearfix">
                                                    <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                                    <h3><a title="" href="single-page-listing.html">Honda Civic 2017 Sports Edition</a></h3>
                                                    <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                                    <ul class="list-unstyled">
                                                        <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                                    </ul>
                                                </div>
                                                <div class="ad-info-1">
                                                    <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                                    <ul class="pull-right">
                                                        <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                        <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- Listing Ad Grid -->
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="grid-style-2">
                                        <!-- Listing Ad Grid -->
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="category-grid-box-1">
                                                <div class="featured-ribbon">
                                                    <span>Featured</span>
                                                </div>
                                                <div class="image">
                                                    <img alt="Carspot" src="{{asset('theme/images/posting/6.jpg')}}" class="img-responsive">
                                                    <div class="ribbon popular"></div>
                                                    <div class="price-tag">
                                                        <div class="price"><span>$205,000</span></div>
                                                    </div>
                                                </div>
                                                <div class="short-description-1 clearfix">
                                                    <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                                    <h3><a title="" href="single-page-listing.html">Honda Civic 2017 Sports Edition</a></h3>
                                                    <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                                    <ul class="list-unstyled">
                                                        <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                                    </ul>
                                                </div>
                                                <div class="ad-info-1">
                                                    <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                                    <ul class="pull-right">
                                                        <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                        <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- Listing Ad Grid -->
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="grid-style-2">
                                        <!-- Listing Ad Grid -->
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="category-grid-box-1">
                                                <div class="featured-ribbon">
                                                    <span>Featured</span>
                                                </div>
                                                <div class="image">
                                                    <img alt="Carspot" src="{{asset('theme/images/posting/7.jpg')}}" class="img-responsive">
                                                    <div class="ribbon popular"></div>
                                                    <div class="price-tag">
                                                        <div class="price"><span>$205,000</span></div>
                                                    </div>
                                                </div>
                                                <div class="short-description-1 clearfix">
                                                    <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                                    <h3><a title="" href="single-page-listing.html">Honda Civic 2017 Sports Edition</a></h3>
                                                    <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                                    <ul class="list-unstyled">
                                                        <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                                    </ul>
                                                </div>
                                                <div class="ad-info-1">
                                                    <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                                    <ul class="pull-right">
                                                        <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                        <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- Listing Ad Grid -->
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="grid-style-2">
                                        <!-- Listing Ad Grid -->
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="category-grid-box-1">
                                                <div class="featured-ribbon">
                                                    <span>Featured</span>
                                                </div>
                                                <div class="image">
                                                    <img alt="Carspot" src="{{asset('theme/images/posting/8.jpg')}}" class="img-responsive">
                                                    <div class="ribbon popular"></div>
                                                    <div class="price-tag">
                                                        <div class="price"><span>$205,000</span></div>
                                                    </div>
                                                </div>
                                                <div class="short-description-1 clearfix">
                                                    <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                                    <h3><a title="" href="single-page-listing.html">Honda Civic 2017 Sports Edition</a></h3>
                                                    <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                                    <ul class="list-unstyled">
                                                        <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                                    </ul>
                                                </div>
                                                <div class="ad-info-1">
                                                    <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                                    <ul class="pull-right">
                                                        <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                        <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- Listing Ad Grid -->
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="grid-style-2">
                                        <!-- Listing Ad Grid -->
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="category-grid-box-1">
                                                <div class="featured-ribbon">
                                                    <span>Featured</span>
                                                </div>
                                                <div class="image">
                                                    <img alt="Carspot" src="{{asset('theme/images/posting/9.jpg')}}" class="img-responsive">
                                                    <div class="ribbon popular"></div>
                                                    <div class="price-tag">
                                                        <div class="price"><span>$205,000</span></div>
                                                    </div>
                                                </div>
                                                <div class="short-description-1 clearfix">
                                                    <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                                    <h3><a title="" href="single-page-listing.html">Honda Civic 2017 Sports Edition</a></h3>
                                                    <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                                    <ul class="list-unstyled">
                                                        <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                        <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                                    </ul>
                                                </div>
                                                <div class="ad-info-1">
                                                    <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                                    <ul class="pull-right">
                                                        <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                        <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- Listing Ad Grid -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Middle Content Box End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Featured End =-=-=-=-=-=-= -->
        <!-- =-=-=-=-=-=-= Buy Or Sale =-=-=-=-=-=-= -->
        <section class="sell-box padding-top-70">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                        <div class="sell-box-grid">
                            <div class="short-info">
                                <h3> Want To Sale Your Car ?</h3>
                                <a href="#">Are you looking for a car?</a>
                                <p>Search your car in our Inventory and request a quote on the vehicle of your choosing. </p>
                            </div>
                            <div class="text-center">
                                <img class="img-responsive wow slideInLeft center-block" data-wow-delay="0ms" data-wow-duration="2000ms" src="{{asset('theme/images/sell.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                        <div class="sell-box-grid">
                            <div class="short-info">
                                <h3> Want To Sale Your Car ?</h3>
                                <a href="#">Do you want to sell your car?</a>
                                <p>Request search your car in our Inventory and a quote on the vehicle of your choosing. </p>
                            </div>
                            <div class="text-center">
                                <img class="img-responsive wow slideInRight center-block" data-wow-delay="0ms" data-wow-duration="2000ms" src="{{asset('theme/images/sell-1.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =-=-=-=-=-=-= Blog Section =-=-=-=-=-=-= -->
        <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
        <section class="section-padding white">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <!-- Heading Area -->
                    <div class="heading-panel">
                        <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                            <!-- Main Title -->
                            <h1>Latest  <span class="heading-color"> Trending</span> Ads</h1>
                            <!-- Short Description -->
                            <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                        </div>
                    </div>
                    <!-- Middle Content Area -->
                    <div class="col-md-12 col-sx-12">
                        <!-- Row -->
                        <ul class="list-unstyled">
                            <!-- Listing Grid -->
                            <li>
                                <div class="well ad-listing clearfix">
                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">
                                        <!-- Image Box -->
                                        <div class="img-box">
                                            <img src="{{asset('theme/images/posting/25.jpg')}}" class="img-responsive" alt="">
                                            <div class="total-images"><strong>8</strong> photos </div>
                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>
                                        </div>
                                        <!-- Ad Status --><span class="ad-status"> Featured </span>
                                        <!-- User Preview -->
                                        <div class="user-preview">
                                            <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-7 col-xs-12">
                                        <!-- Ad Content-->
                                        <div class="row">
                                            <div class="content-area">
                                                <div class="col-md-9 col-sm-12 col-xs-12">
                                                    <!-- Ad Title -->
                                                    <h3><a>2016 Audi A6 2.0T Quattro Premium Plus</a></h3>
                                                    <!-- Ad Meta Info -->
                                                    <ul class="ad-meta-info">
                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>
                                                        <li>15 minutes ago </li>
                                                    </ul>
                                                    <!-- Ad Description-->
                                                    <div class="ad-details">
                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>
                                                        <ul class="list-unstyled">
                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>
                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>
                                                            <li><i class="flaticon-key"></i>Manual</li>
                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 col-sm-12">
                                                    <!-- Ad Stats -->
                                                    <div class="short-info">
                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>
                                                        <div class="ad-stats hidden-xs"><span>Type : </span>Coupe</div>
                                                        <div class="ad-stats hidden-xs"><span>Make : </span>Audi</div>
                                                    </div>
                                                    <!-- Price -->
                                                    <div class="price"> <span>$18,640</span> </div>
                                                    <!-- Ad View Button -->
                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Ad Content End -->
                                    </div>
                                </div>
                            </li>
                            <!-- Listing Grid -->
                            <li>
                                <div class="well ad-listing clearfix">
                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">
                                        <!-- Image Box -->
                                        <div class="img-box">
                                            <img src="{{asset('theme/images/posting/26.jpg')}}" class="img-responsive" alt="">
                                            <div class="total-{{asset('theme/images"><strong>4</strong> photos </div>
                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>
                                        </div>
                                        <!-- Ad Status --><span class="ad-status"> Featured </span>
                                        <!-- User Preview -->
                                        <div class="user-preview">
                                            <a href="#"> <img src="{{asset('theme/images/users/6.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-7 col-xs-12">
                                        <!-- Ad Content-->
                                        <div class="row">
                                            <div class="content-area">
                                                <div class="col-md-9 col-sm-12 col-xs-12">
                                                    <!-- Ad Title -->
                                                    <h3><a>2010 Ford Shelby GT500 Coupe</a></h3>
                                                    <!-- Ad Meta Info -->
                                                    <ul class="ad-meta-info">
                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>
                                                        <li>15 minutes ago </li>
                                                    </ul>
                                                    <!-- Ad Description-->
                                                    <div class="ad-details">
                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>
                                                        <ul class="list-unstyled">
                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>
                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>
                                                            <li><i class="flaticon-key"></i>Manual</li>
                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 col-sm-12">
                                                    <!-- Ad Stats -->
                                                    <div class="short-info">
                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>
                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>
                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Mobiles</div>
                                                    </div>
                                                    <!-- Price -->
                                                    <div class="price"> <span>$900</span> </div>
                                                    <!-- Ad View Button -->
                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Ad Content End -->
                                    </div>
                                </div>
                                </div>
                            </li>
                            <!-- Listing Grid -->
                            <li>
                                <div class="well ad-listing clearfix">
                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">
                                        <!-- Image Box -->
                                        <div class="img-box">
                                            <img src="{{asset('theme/images/posting/7.jpg')}}" class="img-responsive" alt="">
                                            <div class="total-images"><strong>5</strong> photos </div>
                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>
                                        </div>
                                        <!-- User Preview -->
                                        <div class="user-preview">
                                            <a href="#"> <img src="{{asset('theme/images/users/5.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-7 col-xs-12">
                                        <!-- Ad Content-->
                                        <div class="row">
                                            <div class="content-area">
                                                <div class="col-md-9 col-sm-12 col-xs-12">
                                                    <!-- Ad Title -->
                                                    <h3><a> 2010 Lamborghini Gallardo Spyder</a></h3>
                                                    <!-- Ad Meta Info -->
                                                    <ul class="ad-meta-info">
                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>
                                                        <li>15 minutes ago </li>
                                                    </ul>
                                                    <!-- Ad Description-->
                                                    <div class="ad-details">
                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>
                                                        <ul class="list-unstyled">
                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>
                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>
                                                            <li><i class="flaticon-key"></i>Manual</li>
                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 col-sm-12">
                                                    <!-- Ad Stats -->
                                                    <div class="short-info">
                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>
                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>
                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Mobiles</div>
                                                    </div>
                                                    <!-- Price -->
                                                    <div class="price"> <span>$120</span> </div>
                                                    <!-- Ad View Button -->
                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Ad Content End -->
                                    </div>
                                </div>
                            </li>
                            <!-- Listing Grid -->
                            <li>
                                <div class="well ad-listing clearfix">
                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">
                                        <!-- Image Box -->
                                        <div class="img-box">
                                            <img src="{{asset('theme/images/posting/2.jpg')}}" class="img-responsive" alt="">
                                            <div class="total-{{asset('theme/images"><strong>3</strong> photos </div>
                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>
                                        </div>
                                        <!-- User Preview -->
                                        <div class="user-preview">
                                            <a href="#"> <img src="{{asset('theme/images/users/3.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-7 col-xs-12">
                                        <!-- Ad Content-->
                                        <div class="row">
                                            <div class="content-area">
                                                <div class="col-md-9 col-sm-12 col-xs-12">
                                                    <!-- Ad Title -->
                                                    <h3><a> Porsche 911 Carrera 2017 </a></h3>
                                                    <!-- Ad Meta Info -->
                                                    <ul class="ad-meta-info">
                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>
                                                        <li>15 minutes ago </li>
                                                    </ul>
                                                    <!-- Ad Description-->
                                                    <div class="ad-details">
                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>
                                                        <ul class="list-unstyled">
                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>
                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>
                                                            <li><i class="flaticon-key"></i>Manual</li>
                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 col-sm-12">
                                                    <!-- Ad Stats -->
                                                    <div class="short-info">
                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>
                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>
                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Mobiles</div>
                                                    </div>
                                                    <!-- Price -->
                                                    <div class="price"> <span>$1,129</span> </div>
                                                    <!-- Ad View Button -->
                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Ad Content End -->
                                    </div>
                                </div>
                                </div>
                            </li>
                            <!-- Listing Grid -->
                            <li>
                                <div class="well ad-listing clearfix">
                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">
                                        <!-- Image Box -->
                                        <div class="img-box">
                                            <img src="{{asset('theme/images/posting/15.jpg')}}" class="img-responsive" alt="">
                                            <div class="total-images"><strong>4</strong> photos </div>
                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>
                                        </div>
                                        <!-- Ad Status --><span class="ad-status"> Featured </span>
                                        <!-- User Preview -->
                                        <div class="user-preview">
                                            <a href="#"> <img src="{{asset('theme/images/users/7.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-7 col-xs-12">
                                        <!-- Ad Content-->
                                        <div class="row">
                                            <div class="content-area">
                                                <div class="col-md-9 col-sm-12 col-xs-12">
                                                    <!-- Ad Title -->
                                                    <h3><a>Bugatti Veyron Super Sport </a></h3>
                                                    <!-- Ad Meta Info -->
                                                    <ul class="ad-meta-info">
                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>
                                                        <li>15 minutes ago </li>
                                                    </ul>
                                                    <!-- Ad Description-->
                                                    <div class="ad-details">
                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>
                                                        <ul class="list-unstyled">
                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>
                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>
                                                            <li><i class="flaticon-key"></i>Manual</li>
                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 col-sm-12">
                                                    <!-- Ad Stats -->
                                                    <div class="short-info">
                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>
                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>
                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Mobiles</div>
                                                    </div>
                                                    <!-- Price -->
                                                    <div class="price"> <span>$350</span> </div>
                                                    <!-- Ad View Button -->
                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Ad Content End -->
                                    </div>
                                </div>
                            </li>
                            <!-- Listing Grid -->
                            <li>
                                <div class="well ad-listing clearfix">
                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">
                                        <!-- Image Box -->
                                        <div class="img-box">
                                            <img src="{{asset('theme/images/posting/28.jpg')}}" class="img-responsive" alt="">
                                            <div class="total-{{asset('theme/images"><strong>4</strong> photos </div>
                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>
                                        </div>
                                        <!-- Ad Status --><span class="ad-status"> Featured </span>
                                        <!-- User Preview -->
                                        <div class="user-preview">
                                            <a href="#"> <img src="{{asset('theme/images/users/6.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-7 col-xs-12">
                                        <!-- Ad Content-->
                                        <div class="row">
                                            <div class="content-area">
                                                <div class="col-md-9 col-sm-12 col-xs-12">
                                                    <!-- Ad Title -->
                                                    <h3><a>Audi A4 2.0T Quattro Premium</a></h3>
                                                    <!-- Ad Meta Info -->
                                                    <ul class="ad-meta-info">
                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>
                                                        <li>15 minutes ago </li>
                                                    </ul>
                                                    <!-- Ad Description-->
                                                    <div class="ad-details">
                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>
                                                        <ul class="list-unstyled">
                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>
                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>
                                                            <li><i class="flaticon-key"></i>Manual</li>
                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 col-sm-12">
                                                    <!-- Ad Stats -->
                                                    <div class="short-info">
                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>
                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>
                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Computer & Laptops</div>
                                                    </div>
                                                    <!-- Price -->
                                                    <div class="price"> <span>$250</span> </div>
                                                    <!-- Ad View Button -->
                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Ad Content End -->
                                    </div>
                                </div>
                                </div>
                            </li>
                            <!-- Listing Grid -->
                            <li>
                                <div class="well ad-listing clearfix">
                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">
                                        <!-- Image Box -->
                                        <div class="img-box">
                                            <img src="{{asset('theme/images/posting/13.jpg')}}" class="img-responsive" alt="">
                                            <div class="total-images"><strong>4</strong> photos </div>
                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>
                                        </div>
                                        <!-- Ad Status --><span class="ad-status"> Featured </span>
                                        <!-- User Preview -->
                                        <div class="user-preview">
                                            <a href="#"> <img src="{{asset('theme/images/users/7.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-7 col-xs-12">
                                        <!-- Ad Content-->
                                        <div class="row">
                                            <div class="content-area">
                                                <div class="col-md-9 col-sm-12 col-xs-12">
                                                    <!-- Ad Title -->
                                                    <h3><a>Audi Q3 2.0T Premium Plus</a></h3>
                                                    <!-- Ad Meta Info -->
                                                    <ul class="ad-meta-info">
                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>
                                                        <li>15 minutes ago </li>
                                                    </ul>
                                                    <!-- Ad Description-->
                                                    <div class="ad-details">
                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>
                                                        <ul class="list-unstyled">
                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>
                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>
                                                            <li><i class="flaticon-key"></i>Manual</li>
                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 col-sm-12">
                                                    <!-- Ad Stats -->
                                                    <div class="short-info">
                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>
                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>
                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Laptops</div>
                                                    </div>
                                                    <!-- Price -->
                                                    <div class="price"> <span>$440</span> </div>
                                                    <!-- Ad View Button -->
                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Ad Content End -->
                                    </div>
                                </div>
                            </li>
                            <!-- Listing Grid -->
                            <li>
                                <div class="well ad-listing clearfix">
                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">
                                        <!-- Image Box -->
                                        <div class="img-box">
                                            <img src="{{asset('theme/images/posting/27.jpg')}}" class="img-responsive" alt="">
                                            <div class="total-{{asset('theme/images"><strong>4</strong> photos </div>
                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>
                                        </div>
                                        <!-- Ad Status --><span class="ad-status"> Featured </span>
                                        <!-- User Preview -->
                                        <div class="user-preview">
                                            <a href="#"> <img src="{{asset('theme/images/users/4.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-7 col-xs-12">
                                        <!-- Ad Content-->
                                        <div class="row">
                                            <div class="content-area">
                                                <div class="col-md-9 col-sm-12 col-xs-12">
                                                    <!-- Ad Title -->
                                                    <h3><a>2014 Ford Fusion Titanium</a></h3>
                                                    <!-- Ad Meta Info -->
                                                    <ul class="ad-meta-info">
                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>
                                                        <li>15 minutes ago </li>
                                                    </ul>
                                                    <!-- Ad Description-->
                                                    <div class="ad-details">
                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>
                                                        <ul class="list-unstyled">
                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>
                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>
                                                            <li><i class="flaticon-key"></i>Manual</li>
                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 col-sm-12">
                                                    <!-- Ad Stats -->
                                                    <div class="short-info">
                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>
                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>
                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Furniture</div>
                                                    </div>
                                                    <!-- Price -->
                                                    <div class="price"> <span>$110,50</span> </div>
                                                    <!-- Ad View Button -->
                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Ad Content End -->
                                    </div>
                                </div>
                                </div>
                            </li>
                        </ul>
                        <!-- Row End -->
                    </div>
                    <!-- Middle Content Area  End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
        <!-- =-=-=-=-=-=-= Statistics Counter =-=-=-=-=-=-= -->
        <div class="funfacts custom-padding parallex">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="icons">
                            <i class="flaticon-vehicle"></i>
                        </div>
                        <div class="number"><span class="timer" data-from="0" data-to="1238" data-speed="1500" data-refresh-interval="5">0</span>+</div>
                        <h4>Total <span>Cars</span></h4>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="icons">
                            <i class="flaticon-security"></i>
                        </div>
                        <div class="number"><span class="timer" data-from="0" data-to="820" data-speed="1500" data-refresh-interval="5">0</span>+</div>
                        <h4>Verified <span>Dealers</span></h4>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="icons">
                            <i class="flaticon-like-1"></i>
                        </div>
                        <div class="number"><span class="timer" data-from="0" data-to="1042" data-speed="1500" data-refresh-interval="5">0</span>+</div>
                        <h4>Active <span>Users</span></h4>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="icons">
                            <i class="flaticon-cup"></i>
                        </div>
                        <div class="number"><span class="timer" data-from="0" data-to="34" data-speed="1500" data-refresh-interval="5">0</span>+</div>
                        <h4>Featured <span>Ads</span></h4>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.funfacts -->
        <!-- =-=-=-=-=-=-= Statistics Counter End =-=-=-=-=-=-= -->
        <!-- =-=-=-=-=-=-= Blog Section =-=-=-=-=-=-= -->
        <section class="custom-padding">
            <!-- Main Container -->
            <div class="container">
                <!-- Content Box -->
                <!-- Row -->
                <div class="row">
                    <!-- Heading Area -->
                    <div class="heading-panel">
                        <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                            <!-- Main Title -->
                            <h1>Latest <span class="heading-color"> Blog</span> Post</h1>
                            <!-- Short Description -->
                            <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                        </div>
                    </div>
                    <!-- Middle Content Box -->
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="row">
                            <div class="posts-masonry">
                                <!-- Blog Post-->
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <a href="#"> <img class="img-responsive" alt="" src="{{asset('theme/images/posting/15.jpg')}}"> </a>
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#"> 2017 Bugatti Chiron: Again with the Overkill </a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Post-->
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <a href="#"> <img class="img-responsive" alt="" src="{{asset('theme/images/posting/27.jpg')}}"> </a>
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#"> Ford Mustang EcoBoost Premium Convertible</a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Post-->
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <a href="#"> <img class="img-responsive" alt="" src="{{asset('theme/images/posting/9.jpg')}}"> </a>
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#"> 2010 Audi A5 Auto Premium quattro MY10 </a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Grid -->
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- Middle Content Box End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Blog Section End =-=-=-=-=-=-= -->
        <!-- =-=-=-=-=-=-= Car Comparison =-=-=-=-=-=-= -->
        <section class="section-padding gray">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <div class="clearfix"></div>
                    <!-- Heading Area -->
                    <div class="heading-panel">
                        <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                            <!-- Main Title -->
                            <h1>Popular  <span class="heading-color"> Car</span> Comparison</h1>
                            <!-- Short Description -->
                            <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                        </div>
                    </div>
                    <!-- Middle Content Box -->
                    <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12 text-center ">
                            <ul class="compare-masonry">
                                <li class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="comparison-box">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#" class="">
                                                    <img src="{{asset('theme/images/compare/2.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2016 Ford Escape cape  </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vsbox">vs</div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="" class=""><img src="{{asset('theme/images/compare/1.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2017 Chevrolet Camaro </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <li class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="comparison-box">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#" class="">
                                                    <img src="{{asset('theme/images/compare/3.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2017 Chevrolet Corvette </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vsbox">vs</div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="" class=""><img src="{{asset('theme/images/compare/4.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2017 Honda Accord Sedan </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <li class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="comparison-box">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#" class="">
                                                    <img src="{{asset('theme/images/compare/5.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">Mercedes-Benz C-Class </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vsbox">vs</div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="" class=""><img src="{{asset('theme/images/compare/6.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2017 Honda CR-V </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <li class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="comparison-box">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#" class="">
                                                    <img src="{{asset('theme/images/compare/7.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2016 Ford Mustang</a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vsbox">vs</div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="" class=""><img src="{{asset('theme/images/compare/8.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2017 Toyota RAV4 </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="text-center">
                        <div class="load-more-btn">
                            <button class="btn btn-theme"> View All Comparisons <i class="fa fa-refresh"></i> </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =-=-=-=-=-=-= Car Comparison End  =-=-=-=-=-=-= -->
        <!-- =-=-=-=-=-=-= Call to Action =-=-=-=-=-=-= -->
        <div class="parallex bg-img-3  section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <div class="call-action">
                            <i class="flaticon-like-1"></i>
                            <h4>Post featured ad and get great exposure </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </div>
                        <!-- end subsection-text -->
                    </div>
                    <!-- end col-md-8 -->
                    <div class="col-md-4 col-sm-12">
                        <div class="parallex-button"> <a href="#" class="btn btn-theme">Post Free Ad <i class="fa fa-angle-double-right "></i></a> </div>
                        <!-- end parallex-button -->
                    </div>
                    <!-- end col-md-4 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- =-=-=-=-=-=-= Call to Action =-=-=-=-=-=-= -->

    </div>
@endsection
