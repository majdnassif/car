@extends('welcome')

@section('content')

    <!-- =-=-=-=-=-=-= Main Header End  =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
    <div class="page-header-area-2 gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="small-breadcrumb">
                        <div class=" breadcrumb-link">
                            <ul>
                                <li><a href="{{route('index')}}">Home Page</a></li>
                                <li><a class="active" href="#">Registration</a></li>
                            </ul>
                        </div>
                        <div class="header-page">
                            <h1>Create Your Account</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Registration Form =-=-=-=-=-=-= -->

        <section class="section-padding no-top gray">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <!-- Middle Content Area -->
                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
















                        <form id="verificationForm" style="display:none">
                            <h4 style="margin-top: 35px; font-size: 44px; margin-left: 27px; ">  Go To Your Mail </h4>
                            <h4 style="font-size: 44px; margin-top: 6px;">And Get The Code</h4>
                            <div class="form-group input-group" style="margin-top: 5%; margin-left: 10%;">
                                <label>Verification Code</label>
                                <input type="text" class="form-control" id="verf" name="code" placeholder="code" style="width: 100%">

                            </div>
                            <label><a id="resendlabel" style="margin-left: 16rem;">Resend Code</a></label>
                            <button type="submit" class="btn btn-theme btn-lg btn-block" style="width: 33%; margin-top: 39px; margin-left: 110px;">Done</button>
                        </form>

                        <form id="resend" style="display:none">
                        </form>



                        <!--  Form -->
                        <div class="form-grid" id="reg">
                            <form id="RegisterForm">


                                <div class="form-group">
                                    <label>Name</label>
                                    <input placeholder="Enter Your Name" class="form-control" type="text" id="name" name="name">
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input placeholder="Your Email" class="form-control" type="email" id="email_or_phone" name="email_or_phone">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input placeholder="Your Password" class="form-control" type="password" id="password" name="password">
                                </div>


                                <div class="form-group">
                                    <label>Re-enter Password</label>
                                    <input placeholder="Re-enter Password" class="form-control" type="password" id="Rpassword" name="Rpassword">
                                    <label id="Rpassword_error_reg" class="error" for="Rpassword"></label>
                                </div>





                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-7">
                                            <div class="skin-minimal">
                                                <ul class="list">
                                                    <li>
{{--                                                        <input  type="checkbox" id="minimal-checkbox-1" name="checkbox1">--}}
{{--                                                        <label for="minimal-checkbox-1">i agree <a href="#">Terms of Services</a></label>--}}
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-5 text-right">
                                            {{--                                            <p class="help-block"><a data-target="#myModal" data-toggle="modal">Forgot password?</a>--}}
                                            {{--                                            </p>--}}
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-theme btn-lg btn-block" type="submit">Register</button>

                            </form>
                        </div>
                        <!-- Form -->
                    </div>
                    <!-- Middle Content Area  End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Registration Form End =-=-=-=-=-=-= -->
    </div>
@endsection
