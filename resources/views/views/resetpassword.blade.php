@extends('welcome')

@section('content')

    <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
    <div class="page-header-area-2 gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="small-breadcrumb">
                        <div class=" breadcrumb-link">
                            <ul>
                                <li><a href="{{route('index')}}">Home Page</a></li>
                                <li><a class="active" href="#">Login</a></li>
                            </ul>
                        </div>
                        <div class="header-page">
                            <h1>Reset Password </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
        <section class="section-padding no-top gray">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <!-- Middle Content Area -->
                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                        <!--  Form -->
                        <div class="form-grid">
                            <form id="resetpassword">
{{--                                <a class="btn btn-lg btn-block btn-social btn-facebook">--}}
{{--                                    <span class="fa fa-facebook"></span> Sign in with Facebook--}}
{{--                                </a>--}}

{{--                                <a class="btn btn-lg btn-block btn-social btn-google">--}}
{{--                                    <span class="fa fa-google"></span> Sign in with Facebook--}}
{{--                                </a>--}}

{{--                                <h2 class="no-span"><b>(OR)</b></h2>--}}

                                <div class="form-group">
                                    <label>New Password</label>
                                    <input placeholder="Your Password" class="form-control" type="password" id="resetpassword" name="password">
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input placeholder="Confirm Password" class="form-control" type="password" id="password_confirmation" name="password_confirmation">
                                    <label id="password_confirmation_error_reg" class="error" for="password_confirmation"></label>
                                </div>
                                <div class="form-group" style="margin-top: -2rem;">
                                    <label>Confirm Code</label>
                                    <input placeholder="Code" class="form-control" type="text" id="confirm_Code" name="confirm_Code">
                                    <label style="margin-top: 13px; margin-left: 18rem;"><a id="resetlabel" style="margin-left: 21rem; font-size: 18px;">Resend Code</a></label>
                                    {{--                                    <label id="password_confirmation_error_reg" class="error" for="password_confirmation"></label>--}}
                                </div>


                                <button type="submit" class="btn btn-theme btn-lg btn-block">Done</button>
                            </form>
                        </div>
                        <!-- Form -->
                    </div>
                    <!-- Middle Content Area  End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
    </div>

    <form id="resetcode" style="display:none">
    </form>

@endsection
