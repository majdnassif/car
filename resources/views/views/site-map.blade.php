@extends('welcome')

@section('content')

    <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
    <div class="page-header-area-2 gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="small-breadcrumb">
                        <div class=" breadcrumb-link">
                            <ul>
                                <li><a href="index.html">Home Page</a></li>
                                <li><a class="active" href="#">Sitemap</a></li>
                            </ul>
                        </div>
                        <div class="header-page">
                            <h1>Explore Carspot Site</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
        <section class="custom-padding no-top gray">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <div class="posts-masonry">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="site-map">
                                <h3>Home Pages</h3>
                                <ul class="site-map-list">
                                    <li><a href="index.html">Home  - Default </a></li>
                                    <li><a href="index-1.html">Home 1 (Modern)</a></li>
                                    <li><a href="index-2.html">Home 2 (Master Slider)</a></li>
                                    <li><a href="index-3.html">Home 3 (With Map Listing)</a></li>
                                    <li><a href="index-4.html">Home 4 (Owl Carousel)</a></li>
                                    <li><a href="index-5.html">Home 5 (Trending)</a></li>
                                </ul>
                            </div>
                            <!-- .site-map -->
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="site-map">
                                <h3>Grid Listing</h3>
                                <ul class="site-map-list">
                                    <li><a href="listing.html"> Grid Style (Defualt)</a></li>
                                    <li><a href="listing-1.html"> Grid Style 1</a></li>
                                    <li><a href="listing-2.html"> Grid Style 2</a></li>
                                    <li><a href="listing-3.html"> Grid Style 3</a></li>
                                    <li><a href="listing-4.html"> Grid Style 4</a></li>
                                </ul>
                            </div>
                            <!-- .site-map -->
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="site-map">
                                <h3>List View Listing</h3>
                                <ul class="site-map-list">
                                    <li><a href="#">List View 1</a></li>
                                    <li><a href="listing-6.html">List View 2</a></li>
                                    <li><a href="listing-7.html">List View 3</a></li>
                                    <li><a href="listing-8.html">List View 4</a></li>
                                </ul>
                            </div>
                            <!-- .site-map -->
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="site-map">
                                <h3>Single Ad</h3>
                                <ul class="site-map-list">
                                    <li><a href="single-page-listing.html">Single Ad Detail</a></li>
                                    <li><a href="single-page-listing-1.html">Single Ad (Gallery)</a></li>
                                    <li><a href="single-page-listing-2.html">Single Ad (Gallery 2)</a></li>
                                    <li><a href="single-page-listing-3.html">Single Ad Variation</a></li>
                                </ul>
                            </div>
                            <!-- .site-map -->
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="site-map">
                                <h3>Car Reviews</h3>
                                <ul class="site-map-list">
                                    <li><a href="reviews.html">Expert Reviews</a></li>
                                    <li><a href="review-detail.html">Review Detial</a></li>
                                </ul>
                            </div>
                            <!-- .site-map -->
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="site-map">
                                <h3>Comparison</h3>
                                <ul class="site-map-list">
                                    <li><a href="compare.html">Car Comparison</a></li>
                                    <li><a href="compare-1.html">Comparison Style 2</a></li>
                                    <li><a href="compare-2.html">Comparison Detial</a></li>
                                </ul>
                            </div>
                            <!-- .site-map -->
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="site-map">
                                <h3>Dashboard </h3>
                                <ul class="site-map-list">
                                    <li><a href="profile.html">User Profile</a></li>
                                    <li><a href="archives.html">Archives</a></li>
                                    <li><a href="active-ads.html">Active Ads</a></li>
                                    <li><a href="favourite.html">Favourite Ads</a></li>
                                    <li><a href="messages.html">Message Panel</a></li>
                                    <li><a href="deactive.html">Account Deactivation</a></li>
                                </ul>
                            </div>
                            <!-- .site-map -->
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="site-map">
                                <h3>Blog</h3>
                                <ul class="site-map-list">
                                    <li><a href="blog.html"> Right Sidebar</a></li>
                                    <li><a href="blog-1.html"> Masonry Style</a></li>
                                    <li><a href="blog-2.html"> Without Sidebar</a></li>
                                    <li><a href="blog-details.html">Single Blog </a></li>
                                </ul>
                            </div>
                            <!-- .site-map -->
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="site-map">
                                <h3>Template Pages</h3>
                                <ul class="site-map-list">
                                    <li><a href="about.html">About Us</a></li>
                                    <li><a href="about-1.html">About Us 2</a></li>
                                    <li><a href="cooming-soon.html">Comming Soon</a></li>
                                    <li><a href="elements.html">Shortcodes</a></li>
                                    <li><a href="post-ad-1.html">Post Ad</a></li>
                                    <li><a href="pricing.html">Pricing</a></li>
                                    <li><a href="site-map.html">Site Map</a></li>
                                    <li><a href="contact.html">Contact Us</a></li>
                                    <li><a href="services.html">Services</a></li>
                                    <li><a href="services-1.html">Services 2</a></li>

                                </ul>
                            </div>
                            <!-- .site-map -->
                        </div>
                    </div>
                </div>
                <!-- Row End -->
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
    </div>
@endsection
